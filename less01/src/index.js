import React from 'react';
import ReactDOM from 'react-dom';



function Table(){
  return (
    <tbody id="table">
<h2>Знаки задіаку</h2>
  <tr>
  <td>Овен</td>
 
  <td>21 березня – 19 квітня</td>
  </tr>
 <tr>
  <td>Телець</td>
  <td>20 квітня – 20 травня</td>
 </tr>
 <tr>
  <td>Близнюки</td>
  <td>21 травня – 20 червня</td>
 </tr>
 <tr>
  <td>Рак</td>
  <td>21 червня – 22 липня</td>
 </tr>
 <tr>
  <td>Лев</td>
  <td>23 липня- 22 серпня</td>
 </tr>
 <tr>
  <td>Діва</td>
  <td>23 серпня – 22 вересня</td>
 </tr>
 <tr>
  <td>Терези</td>
  <td>23 вересня – 22 жовтня</td>
 </tr>
 <tr>
  <td>Скорпіон</td>
  <td>23 жовтня – 21 листопада</td>
 </tr>
 <tr>
  <td>Стрілець</td>
  <td>23 листопада - 22 грудня</td>
 </tr>
 <tr>
  <td>Козеріг</td>
  <td>23 грудня - 20 січня</td>
 </tr>
 <tr>
  <td>Водолій</td>
  <td>21 грудня - 19 лютого</td>
 </tr>
 <tr>
  <td>Риби</td>
  <td>20 лютого - 20 березня</td>
 </tr>
    </tbody>
 

  )
}

ReactDOM.render(<Table></Table>, document.getElementById('root'))
