import "./card.css";
import React from "react";

function Card(props) {
  const data = props.myBuy;
  const saveState = props.set;

  function deleteBuy(e) {
    let f = parseInt(e.target.getAttribute("data-id"));
    let index = data.findIndex((e) => e.id === f);
    data.splice(index, 1);
    saveState(data);
  }

  const editBuy = (e)=>{
    const area = document.getElementById("text");
    area.className = ''
    let f = parseInt(e.target.getAttribute("data-id"));
    let index = data.findIndex((e) => e.id === f);
    area.value = data[index].text
    data.splice(index, 1);
    saveState(data);

  }

  return data.map(({ id, text }) => {
    return (
      <div
        className="card"
        key={id}
      >
        <div
          className="text"
          id={id}
        >
          {text}
        </div>
        <div>
          <button data-id={id} onClick={editBuy}>✍️</button>
        </div>
        <div>
          <button
            data-id={id}
            onClick={deleteBuy}
          >
            ❌
          </button>
        </div>
        <div>
          <input
            datatype={id}
            type="checkbox"
            onClick={() => {
              const textBuy = document.getElementById(id);
              textBuy.classList.toggle("buy");
            }}
          />
        </div>
      </div>
    );
  });
}

export default Card;
