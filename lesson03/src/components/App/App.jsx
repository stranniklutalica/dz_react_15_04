import React from "react";
import Card from "../Card/Card";
import "./App.css";

class App extends React.Component {
  state = {
    data: [],
  };

  clickBtn() {
    const hide = document.getElementById("area");
    hide.className = "";
  }

  clickSave = () => {
    const randomId = Date.now();
    const text = document.getElementById("text");
    let data = this.state.data;

    data.push({ id: randomId, text: text.value });
    this.setState((state) => {
      return {
        ...state,
        data: data,
      };
    });
  };

  saveState = (e) => {
    this.setState((state) => {
      return {
        ...state,
        data: e,
      };
    });
  };

  render() {
    return (
      <div className="app">
        <div className="data">
          <button
            className="btn"
            onClick={this.clickBtn}
          >
            Додати ➕
          </button>
          <div
            id="area"
            className="hide"
          >
            <textarea
              id="text"
              cols="40"
              rows="5"
            ></textarea>
            <div>
              <button
                className="btn"
                onClick={this.clickSave}
              >
                Зберегти 📀
              </button>
            </div>
          </div>
        </div>
        <div id="cards">
          <Card
            myBuy={this.state.data}
            set={this.saveState}
          ></Card>
        </div>
      </div>
    );
  }
}

export default App;
