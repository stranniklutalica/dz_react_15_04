import React from "react";
import "./Cards.css";

function Cards(props) {
  const data = props.data;
  const key = props.data.id;

  return data.map((item) => {
    return (
      <div
        className="card"
        key={key}
      >
        <div>
          <div className="title">{item.title}</div>
        </div>
        <div>
          <img
            className="image_product"
            src={item.image}
            alt="product"
          />
        </div>
        <div>
          <div className="rating">
            <span className="span">Rating: </span>
            {item.rating.rate}
          </div>
          <div className="price">
            <span className="span">Price: </span> {item.price} $
          </div>
          <div className="desc">{item.description}</div>
        </div>
      </div>
    );
  });
}

export default Cards;
