import React from "react";
import "./App.css";
import Cards from "../Cards/Cards";

class App extends React.Component {
  state = {
    data: [],
  };

  componentDidMount() {
    fetch("https://fakestoreapi.com/products")
      .then((resp) => resp.json())
      .then((rezult) =>
        this.setState({
          data: rezult,
        })
      );
  }

  render() {
    return (
      <div className="app">
        <Cards data={this.state.data}></Cards>
      </div>
    );
  }
}

export default App;
