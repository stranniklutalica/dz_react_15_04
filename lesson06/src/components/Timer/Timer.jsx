import TimerControls from "../TimerControls/TimerControls";
import TimerDisplay from "../TimerDisplay";
import { useState, useEffect } from "react";

export default function Timer() {
  const [timerMin, setTimerMin] = useState(0);
  const [timerSec, setTimerSec] = useState(0);
  const [status, setStatus] = useState(true);
  const [time, setTime] = useState({ s: 0, m: 0 });
  const [interv, setInterv] = useState();

  useEffect(() => {
    setTime({ s: timerSec, m: timerMin });
  }, [timerMin, timerSec]);

  const startBtn = () => {
    run();
    setStatus(false);
    setInterv(setInterval(run, 1000));
  };

  let updatedS = time.s,
    updatedM = time.m;

  if (updatedS <= 0 && updatedM <= 0) {
    updatedS = 0;
    updatedM = 0;

    clearInterval(interv);
  }

  const run = () => {
    if (updatedS !== 0) {
      updatedS--;
    }

    if (updatedS === 0 && updatedM !== 0) {
      updatedS = 59;
      updatedM--;
    }
    if (updatedS === 0 && updatedM === 0) {
      finish();
    }
    return setTime({ s: updatedS, m: updatedM })
  };

  const finish = () => {
    console.log("fin");
    setStatus(true);
  };

  const stopBtn = () => {
    clearInterval(interv);
  };

  const resetBtn = () => {
    clearInterval(interv);
    setTimerMin(0);
    setTimerSec(0);
    setStatus(true);
  };

  return (
    <>
      <TimerDisplay
        minutes={time.m}
        seconds={time.s}
      />
      <TimerControls
        start={startBtn}
        stop={stopBtn}
        reset={resetBtn}
        setTimerMin={setTimerMin}
        setTimerSec={setTimerSec}
        status={status}
      />
    </>
  );
}
