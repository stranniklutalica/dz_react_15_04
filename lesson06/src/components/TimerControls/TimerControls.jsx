export default function TimerControls(props) {
  
  return (
    <>
      <div className="input_display">
        {props.status ? (
          <input
            type="text"
            placeholder="minute"
            
            onChange={(e) => {
              props.setTimerMin(e.target.value);
            }}
          />
        ) : (
          <input
            type="text"
            placeholder="minute"
           
            disabled
          />
        )}
        {props.status ? (
          <input
            type="text"
            placeholder="second"
            onChange={(e) => {
               props.setTimerSec(e.target.value);
              
            }}
          />
        ) : (
          <input
            type="text"
            placeholder="second"
            disabled
          />
        )}
      </div>
      <div className="btn">
        <button onClick={props.start}>Start</button>
        <button onClick={props.stop}>Stop</button>
        <button onClick={props.reset}>Reset</button>
      </div>
    </>
  );
}
