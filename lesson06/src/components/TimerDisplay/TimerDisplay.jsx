export default function TimerDisplay(props) {

   return (
    <>
      <div>
        <div>
          <span>
            {/* {String(Math.floor(props.fullTime / 60)).padStart(2, "0")} */}
            {props.minutes < 10 ? "0" + props.minutes : props.minutes}
          </span>
          <span>:</span>
          <span>
            {/* {String(props.fullTime % 60).padStart(2, "0")} */}
            {props.seconds < 10 ? "0" + props.seconds : props.seconds}
          </span>
        </div>
      </div>
    </>
  );
}
