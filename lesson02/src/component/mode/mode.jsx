import React from "react";
import "./mode.css";

export default function Mode() {
  return (
    <div className="menu">
      <div>
        <img  className=""        
          src='../images/ligth.png'
          alt='ligth'
        />
      </div>
      <div className="link flex hide">
        <a className="" href="#">Light mode</a>
        <div class="switch-btn">
          <input type="image" src="../images/moon.png"/>
        </div>
      </div>
    </div>
  );
}