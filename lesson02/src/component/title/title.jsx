import React from "react";
import "./title.css";

export default function Title() {
  return (
    <div className="title">
      <div className="name">af</div>
      <div className="name_email hide">
        <p>AnimatedFred</p>
        <a
          href="#"
          type="email"
        >
          animated@demo.com
        </a>
      </div>
      <div className="arrow">
        <img src="../images/arrow.png" alt="arrow"/>
      </div>
    </div>
  );
}
