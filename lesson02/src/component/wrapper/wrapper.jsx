import React from "react";
import "./wrapper.css";
import Title from "../title/title";
import Menu from "../menu/Menu";
import Logout from "../logout/logout";
import Mode from "../mode/mode";

function Wrapper() {
  return (
    <div className="wrapper">
      <Title></Title>
      <Menu
        text="Search..."
        src="../images/search.png"
      ></Menu>
      <Menu
        text="Dashboard"
        src="../images/icon.png"
      ></Menu>
      <Menu
        text="Revenue"
        src="../images/barchart.png"
      ></Menu>
      <Menu
        text="Notification"
        src="../images/notification.png"
      ></Menu>
      <Menu
        text="Analytics"
        src="../images/piechart.png"
      ></Menu>
      <Menu
        text="Inventory"
        src="../images/package.png"
      ></Menu>
      <Logout></Logout>
      <Mode></Mode>
    </div>
  );
}

export default Wrapper;
