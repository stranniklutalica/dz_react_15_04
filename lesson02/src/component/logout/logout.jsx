import React from "react";
import "./logout.css";

export default function Logout() {
  return (
    <div className="menu logout">
      <div>
        <img
          src="../images/logout.png"
          alt="logout"
        />
      </div>
      <div className="link hide">
        <a href="#">Logout</a>
      </div>
    </div>
  );
}
