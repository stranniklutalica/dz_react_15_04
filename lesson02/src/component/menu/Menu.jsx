import React from "react";
import "./menu.css";

export default function Menu({ text, src }) {
  return (
    <div className="menu">
      <div>
        <img
          src={src}
          alt={text}
        />
      </div>
      <div className="link hide">
        <a href="#">{text}</a>
      </div>
    </div>
  );
}
