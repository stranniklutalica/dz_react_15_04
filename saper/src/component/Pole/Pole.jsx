import { useState } from "react";
import "./pole.css";
//import bomb  from "../code/bomb";




export default function Pole() {

const mine='x'
const size = 8
const bomb = 10



function createPole(size){
 const pole = new Array(size*size).fill(0)

 function bombPole(x,y){
  if(x>=0 && x<size && y>=0 && y< size){
    if(pole[y*size+x]=== mine) return
    pole[y*size+x] +=1
  }

 }

 for(let i=0; i<bomb;){
  const x=Math.floor(Math.random()*size)
  const y=Math.floor(Math.random()*size)
  if(pole[y*size+x]===mine) continue
  pole[y*size+x]=mine
  i+=1
  bombPole(x+1,y)
  bombPole(x-1,y)
  bombPole(x,y+1)
  bombPole(x,y-1)
  bombPole(x+1,y-1)
  bombPole(x-1,y-1)
  bombPole(x+1,y+1)
  bombPole(x-1,y+1)
 }

return pole
}

const [displayPole, setDisplayPole] = useState(()=>createPole(size))
const [death, setDeath] = useState(false)
const arr = new Array(size).fill(0)

function click(x,y,e){
  e.target.innerText = displayPole[y*size+x]
  if(displayPole[y*size+x]===mine){
    setDeath(true)
  }
  e.target.setAttribute('data', 'open')

}
  return (
    
    <>
      <div>
        <div className="flex" >
          {death ? (
            arr.map((_,y)=>{
              return(
                <div className="flex1" key={y}>
                  {
                    arr.map((_,x)=>{
                      return (
                        <div className="pole" key={x}>{displayPole[y*size+x]}</div>
                      )
                    })
                  }
                </div>
              )
            })
          ):(
            arr.map((_,y)=>{
              return(
                <div className="flex1" key={y}>
                  {
                    arr.map((_,x)=>{
                      return (
                        <div className="pole" key={x} onClick={(e)=>{click(x,y,e)}} data='closed'></div>
                      )
                    })
                  }
                </div>
              )
            })
          )}
         {
          
         }
        </div>
      </div>
    </>
  );
}


// {displayPole[y*size+x]}