export default function bomb(col) {
  const arrBomb = [];

  while (arrBomb.length < col) {
    const xbomb = Math.floor(Math.random() * 8);
    const ybomb = Math.floor(Math.random() * 8);

    if (!arrBomb.includes(String(xbomb) + ybomb)) {
      arrBomb.push(String(xbomb) + ybomb);
    }
  }

  return arrBomb;
}
