
import Title from "../../pages/title";
import Characters from "../../pages/characters";
import Locations from "../../pages/locations";
import Episodes from "../../pages/episodes";
import { Routes, Route } from "react-router-dom";
import Navigation from "../Navigation/Navigation";
import Person from "../../pages/person/person";
import CardLocation from "../CardLocation/CardLocation";
import CardEpisode from "../CardEpisode/CardEpisode";




function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Navigation></Navigation>}>
          <Route
          index
          element={<Title></Title>}
        ></Route>
        <Route
          path="characters"
          element={<Characters></Characters>}
        ></Route>
        <Route path="characters/person/:name" element={<Person></Person>}></Route>
        
        <Route
          path="locations"
          element={<Locations></Locations>}
        ></Route>
         <Route
          path="locations/:id"
          element={<CardLocation></CardLocation>}
        ></Route>
   
        <Route
          path="episodes"
          element={<Episodes></Episodes>}
        ></Route>
        <Route
          path="episodes/:id"
          element={<CardEpisode></CardEpisode>}
        ></Route>
        </Route>
        
      </Routes>
    </>
  );
}

export default App;
