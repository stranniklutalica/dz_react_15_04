import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { useParams } from "react-router";

export default function Card() {

  const [reqData, setReqData] = useState();
  const id = useParams().name;
const navigate =  useNavigate()
const back = ()=> navigate(-1)

  useEffect(() => {
    fetch("https://rickandmortyapi.com/api/character/" + id)
      .then((res) => res.json())
      .then((data) => setReqData(data));
  },[id]);

  return reqData !== undefined ?(
    <>
      <div className="person">
        <div className="name">{reqData.name}</div>
        <div className="image">
          <img
            src={reqData.image}
            alt={reqData.name}
          />
        </div>
        {/* <div className="episode">
          <Link to={`/episode/person/${id}`}>Episode</Link>
        </div> */}
        <div className="location">
          Location: {reqData.location.name}
                  </div>
        <div>Species: {reqData.species}</div>
        <div>Gender: {reqData.gender}</div>
        <button className="btn-card" onClick={back}>Back</button>
      </div>
      
    </>
  ): null
}
