
import { Link, NavLink, Outlet } from 'react-router-dom'
import './Navigation.css'
import home from './logo.png'
export default function Navigation(){
    return (
      <>
      <div className='nav'>
            <div className="home">
                  <Link to="/"><img src={home} alt="home" /></Link>
            </div>
          <div className="menu-title">
              <NavLink className='click' to="/characters">characters</NavLink>
            <NavLink className='click' to="/locations">locations</NavLink>
            <NavLink className='click' to="/episodes">episodes</NavLink>
          </div>
          
           
            
        </div>
      <Outlet></Outlet>
      </>
        
    )
}
