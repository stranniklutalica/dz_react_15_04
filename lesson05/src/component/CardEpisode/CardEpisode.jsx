import { useState, useEffect } from "react";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";

export default function CardEpisode() {
  const navigate =  useNavigate()
const back = ()=> navigate(-1)
  const [reqData, setReqData] = useState();

  const id = useParams().id;
  const url = "https://rickandmortyapi.com/api/episode/";
  function req(url) {
    fetch(url)
      .then((res) => res.json())
      .then((data) => setReqData(data));
  }
  useEffect(() => {
    req(url + id);
  }, [id]);

  return reqData !== undefined ? (
    <>
      <div className="card">
      <div className="name">{reqData.episode}</div>
        <div className="name">{reqData.name}</div>
        <div>Air date: {reqData.air_date}</div>
       
        {/* <div>Characters:</div>
        {reqData.characters.map((item) => {

          return (
            <>
              <div>
                <Link>{item}</Link>
              </div>
            </>
          );
        })} */}
        <button className="btn-card" onClick={back}>Back</button>
      </div>
    </>
  ) : null;
}
