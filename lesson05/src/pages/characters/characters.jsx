import { Link } from "react-router-dom";
import "./characters.css";

import { useEffect, useState } from "react";

export default function Characters() {
  const url = "https://rickandmortyapi.com/api/character/";
  const [reqData, setReqData] = useState();

  function req(url) {
    fetch(url)
      .then((res) => res.json())
      .then((data) => setReqData(data));
  }

  useEffect(() => {
    req(url);
  }, []);

  function next() {
    if (reqData.info.next) {
      req(reqData.info.next);
    }
  }

  function previous() {
    if (reqData.info.prev) {
      req(reqData.info.prev);
    }
  }

  return reqData !== undefined ? (
    <>
      <div className="cards">
        {reqData.results.map((item) => {
          console.log(item);
          return (
            <>
              <Link
                lassName="click"
                to={`person/${item.id}`}
              >
                <div
                  className="card"
                  id={item.id}
                >
                  <div className="name">{item.name}</div>
                  <div className="image">
                    <img
                      src={item.image}
                      alt={item.name}
                    />
                  </div>
                </div>
              </Link>
            </>
          );
        })}
      </div>
      <div className="btn-box">
        <button
          className="btn"
          onClick={previous}
        >
          Previous
        </button>

        <button
          className="btn"
          onClick={next}
        >
          Next
        </button>
      </div>
    </>
  ) : null;
}
