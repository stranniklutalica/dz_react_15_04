import { Link } from "react-router-dom";
import rick from "./rick-and-morty.png";
import './title.css'

export default function Title(title) {
  return (
    <>
      <div className="titleimg">
        <img
          src={rick}
          alt="rick"
        />
      </div>

      <div className="menu">
        <div className="card">
          <Link
            className="click"
            to="/characters"
          >
            Characters
          </Link>
        </div>
        <div className="card">
          <Link
            className="click"
            to="/locations"
          >
            Locations
          </Link>
        </div>
        <div className="card">
          <Link
            className="click"
            to="/episodes"
          >
            Episodes
          </Link>
        </div>
      </div>
    </>
  );
}
